﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
      static class  Services
    {

        private static IEnumerable<User> allUsers = new List<User>()
        {
           new User{FirstName = "Jerry", LastName = "Agu", Age = 26, Email = "jerry@sun.com"},
           new User{FirstName = "Tony", LastName = "Eze", Age = 23, Email = "tony@xyz.com"},
           new User{FirstName = "Tosin", LastName = "Obato", Age = 22, Email = "obatotosin@gmail.com"},
           new User{FirstName = "Jibril", LastName = "Atahiru", Age = 33, Email = "jibril@sun.com"},
           new User{FirstName = "Kristene", LastName = "Dimarco", Age = 30, Email = "dimarco@sun.com"},
        };

        private static List<Credentials> allUsersCredentials = new List<Credentials>()
        {
           new Credentials{ Email = "jerry@sun.com", Password = "pop"},
           new Credentials{Email = "tony@xyz.com", Password = "pap"},
           new Credentials{Email = "obatotosin@gmail.com", Password = "doe"},
           new Credentials{Email = "jibril@sun.com", Password = "fly"},
        };

        public static async Task< bool> LoginAsync(string email, string password)
        {
            Console.WriteLine("Login reporting...");

            try
            {
                await Task.Delay(8000);
                var user = allUsersCredentials.Single(u => u.Email == email && u.Password == password);
                

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Beep();
                Console.WriteLine("Login Done reporting..");

                if (user.Email == email && user.Password == password)
                    return true;
                return false;
            }
            catch (Exception ex)
            {

               // Console.WriteLine(ex.Message);
                return false;
            }
           
            
            


            


        }

        public async static Task<User> GetUserDetailsAsync(string email)
        {
            
            var user = allUsers.Single(e => e.Email == email);
            Console.WriteLine("getuser reporting...");
            await Task.Delay(4000);


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Beep();
            Console.WriteLine("Get User Done reporting...");
            return user;
        }

    }
}
