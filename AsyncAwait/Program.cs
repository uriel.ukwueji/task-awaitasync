﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
       public  static void Main(string[] args)
        {

            Getting();



            Console.WriteLine("waiting for response");
            Console.ReadKey();
        }



  


        public async  static void Getting()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("chilling for verification...");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(  "Enter your Email");
            var email = RePrompt(Console.ReadLine());

            Console.WriteLine("Enter your Password");
            var password = RePrompt(Console.ReadLine());

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            var getValidTask = Services.LoginAsync(email, password);
            var getProfileTask = Services.GetUserDetailsAsync(email);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("waiting for the two tasks");
            var userDetails = await getProfileTask;
            var valid = await getValidTask; 
            if (valid)
            {
                Console.WriteLine("\n");

                
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($" {userDetails.FirstName}'s Profile: \n ");
                Console.WriteLine($"First Name:  {userDetails.FirstName}");
                Console.WriteLine($"Last Name:  {userDetails.LastName}");
                Console.WriteLine($"Age:  {userDetails.Age}");
                Console.WriteLine($"Email:  {userDetails.Email}");
            }

            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine("Invalid User");
                Console.ResetColor();
            }

            Console.ReadKey();
        }

        public static string RePrompt(string input)
        {
            while (String.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine( "Field cannot be Blank");
                input = Console.ReadLine();

            }
            return input;
        }




    }
}
