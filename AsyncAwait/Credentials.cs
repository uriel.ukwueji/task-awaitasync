﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Credentials : ICredentials<Credentials>
    {
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
